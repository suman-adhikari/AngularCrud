import {Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import {Student} from './student';
import { HomeService } from './home.service';

@Component({
    moduleId: module.id,
    selector:'home',
    templateUrl:'home.component.html'
})

export class HomeComponent implements OnInit { 
    
    isEdit: boolean= false;
    student: Student[]=[];
    std: Student= new Student();
    constructor(public homeService: HomeService){ }

    studentform = new FormGroup({
        id: new FormControl(),
        name: new FormControl(),
        address: new FormControl()
    });
    

    OnInit(){  
      this.getData();
    }

    getData(){
         this.student = this.homeService.GetData();
    }

    onEdit(id:number){     
        
        this.std = this.homeService.UpdateData(id);       
        this.studentform.patchValue({
        id: this.std.id,
        name:this.std.name,
        address:this.std.address
        });
        this.isEdit = true;
    }

    onDelete(id:number){
       this.homeService.delete(id); 
       this.getData();    
    }

   Save(model:Student){       
        this.homeService.SaveData(model,this.isEdit);    
        this.isEdit=false;    
        this.studentform.reset(); 
        this.getData();
        
   }

}




