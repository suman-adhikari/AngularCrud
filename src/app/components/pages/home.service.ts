import {Injectable}    from '@angular/core';
import {Http,Response} from '@angular/http';
import {Observable} from 'rxjs/Rx';

import {Student} from './student';

@Injectable()
export class HomeService{
 
  student:Student[]=[];

  SaveData(model:Student, isEdit:boolean){
      if(isEdit){
         for(var i=0;i<this.student.length;i++){
           if(this.student[i].id==model.id){
               this.student[i].name = model.name;
               this.student[i].address = model.address
           }
        }
      }
      else this.student.push(model);
  }

  GetData(){
     return this.student;
  }

  UpdateData(id:number){
     for(var i=0;i<this.student.length;i++){
           if(this.student[i].id==id){
               return this.student[i];
           }
     }
  }

  delete(id:number){
      for(var i=0;i<this.student.length;i++){
           if(this.student[i].id==id){
               this.student.splice(i, 1);
               break;
           }
       }  
  }

}