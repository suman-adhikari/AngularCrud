import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {BodyComponent} from './body.component';

@NgModule({
    imports:[BrowserModule],
    declarations:[NavbarComponent],
    bootstrap:[NavbarComponent]
})

export class BodyModule{
    
}