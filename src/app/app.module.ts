import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';

import {AppComponent} from './app.component';
import {NavbarComponent} from './components/navbar/navbar.component';
import {BodyComponent} from './components/body/body.component';
import {HomeComponent} from './components/pages/home.component';
import {AboutComponent} from './components/pages/about.component';

import {HomeService} from './components/pages/home.service';

import { RouterModule }   from '@angular/router';
//import {RoutingModule} from './app.routing.module';

@NgModule({
    imports:[BrowserModule,
            RouterModule.forRoot([
                {
                    path:'',
                    component:HomeComponent
                },
                {
                    path:'about',
                    component:AboutComponent
                }
            ]),           
            ReactiveFormsModule
            ],
    declarations:[AppComponent,NavbarComponent, BodyComponent,HomeComponent,AboutComponent],
    providers:[HomeService],
    bootstrap:[AppComponent]
})

export class AppModule{
    
}