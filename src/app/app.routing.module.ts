import {NgModule} from '@angular/core';
import {RouterModule,Routes} from '@angular/router';


import {HomeComponent} from './components/pages/home.component';
import {AboutComponent} from './components/pages/about.component';

const routes: Routes = [
    {
        path:'',
        component:HomeComponent
    },
    {
       path:'about',
       component:AboutComponent
    }
];

@NgModule({
    imports:[RouterModule.forRoot(routes)],
    exports:[RouterModule]
})

exports class RoutingModule{

}